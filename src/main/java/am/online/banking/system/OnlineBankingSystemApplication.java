package am.online.banking.system;

import am.online.banking.system.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.math.BigInteger;
import java.util.UUID;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class OnlineBankingSystemApplication {

    public static void main(String[] args) {

        SpringApplication.run(OnlineBankingSystemApplication.class, args);

//            String password = "admin";
//            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//            String hashedPassword = passwordEncoder.encode(password);
//
//            System.out.println(hashedPassword);
    }
}


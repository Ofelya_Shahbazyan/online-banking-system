package am.online.banking.system.security;

import am.online.banking.system.util.ResponseStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    public CustomAuthenticationFailureHandler() {
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {

        int errorCode;
        String message = "";

        if (exception.getClass().equals(BadCredentialsException.class)
                || exception.getClass().equals(ProviderNotFoundException.class)
                || exception.getClass().equals(UsernameNotFoundException.class)) {
            message = "Bad Credentials";
            errorCode = ResponseStatus.USER_BAD_CREDENTIALS.getCode();
        }
//        else if (exception.getClass().equals(LockedException.class)) {
//            errorCode = ResponseStatus.USER_LOCKED.getCode();
//            message = "locked";
//
//        } else if (exception.getClass().equals(AccountExpiredException.class)) {
//            errorCode = ResponseStatus.UNAUTHORIZED_REQUEST.getCode();
//
//            message = "Cannot find a user";
//
//        } else if (exception.getClass().equals(AuthenticationServiceException.class)) {
//            errorCode = ResponseStatus.USER_AUTHENTICATION_ERROR.getCode();
//            message = "Cannot find a user";
//
//        } else if (exception.getClass().equals(AuthenticationServiceException.class)) {
//            errorCode = ResponseStatus.INTERNAL_ERROR.getCode();
//            message = "Cannot find a user";
//        } else if (exception.getClass().equals(InternalAuthenticationServiceException.class)) {
//            errorCode = ResponseStatus.USER_BAD_CREDENTIALS.getCode();
//            message = "Bad Credentials";
//        }
        else {
            message = "Bad Credentials";
            errorCode = ResponseStatus.USER_BAD_CREDENTIALS.getCode();
        }
        System.out.println(message);

        request.getRequestDispatcher(String.format("/login/error?error=%s", errorCode)).forward(request, response);
    }
}


package am.online.banking.system.controller;

import am.online.banking.system.bean.dto.NotificationDto;
import am.online.banking.system.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/notification")
@Secured("ROLE_USER")
public class NotificationController {

    private NotificationService notificationService;

    @Autowired
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/get/all")
    public ModelAndView getNotification() {
        ModelAndView modelAndView = new ModelAndView();
        List<NotificationDto> notificationDtoList = notificationService.getNotificationByRecipient();
//        if(notificationDtoList.isEmpty()){
//            return ResponseEntity.ok().body("There are no notifications.");
//        }
        modelAndView.addObject("notifications", notificationDtoList);
        modelAndView.setViewName("notification");
        return modelAndView;
    }

}

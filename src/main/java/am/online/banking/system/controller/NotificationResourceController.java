package am.online.banking.system.controller;

import am.online.banking.system.bean.dto.NotificationDto;
import am.online.banking.system.service.NotificationResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/notification/resource")
@Secured("ROLE_ADMIN")
public class NotificationResourceController {

    private NotificationResourceService notificationResourceService;

    @Autowired
    public NotificationResourceController(NotificationResourceService notificationResourceService) {
        this.notificationResourceService = notificationResourceService;
    }

    @GetMapping("/get/all")
    public ModelAndView getNotification() {
        ModelAndView modelAndView = new ModelAndView();
        List<NotificationDto> notificationDtoList = notificationResourceService.getAllNotifications();
//        if(notificationDtoList.isEmpty()){
//            return ResponseEntity.ok().body("There are no notifications.");
//        }
        modelAndView.addObject("notifications", notificationDtoList);
        modelAndView.setViewName("notification");
        return modelAndView;
    }

}

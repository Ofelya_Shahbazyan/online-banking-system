package am.online.banking.system.controller;

import am.online.banking.system.bean.dto.CreateUserDto;
import am.online.banking.system.bean.entity.User;
import am.online.banking.system.mapper.UserMapper;
import am.online.banking.system.service.UserService;
import am.online.banking.system.util.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/user/resource")
public class UserResourceController {

    private UserService userService;
    private UserMapper userMapper;
    private CurrentUser currentUser;

    @Autowired
    public UserResourceController(UserService userService, UserMapper userMapper, CurrentUser currentUser) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.currentUser = currentUser;
    }

    @GetMapping("/create/user")
    public ModelAndView showUserCreateForm() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", new CreateUserDto());
        modelAndView.setViewName("user_create");
        return modelAndView;
    }

    @PostMapping("/save/user")
    public ModelAndView saveNewUser(@Valid @ModelAttribute("user") CreateUserDto createUserDto, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        User user = userMapper.map(createUserDto, User.class);
        if (!createUserDto.getPassword().equals(createUserDto.getMatchingPassword())) {
            bindingResult.rejectValue("matchingPassword", "", "*Password not matching");
            modelAndView.setViewName("user_create");
        } else if (bindingResult.hasErrors()) {
            modelAndView.addObject("user", createUserDto);
            modelAndView.setViewName("user_create");
        } else {
            userService.createUser(user);
            modelAndView.addObject("process", "SUCCESS");
            modelAndView.addObject("pw_success", "User successfully created!");
            User admin = currentUser.getCurrentUser();
            modelAndView.addObject("firstName", admin.getFirstName());
            modelAndView.addObject("tab", " ");
            modelAndView.addObject("lastName", admin.getLastName());
            modelAndView.setViewName("admin_home");
        }
        return modelAndView;
    }

    @GetMapping("/user/all")
    public ModelAndView getUserList() {
        ModelAndView modelAndView = new ModelAndView();
        List<User> userList = userService.findAllUsers();
        modelAndView.addObject("userList", userList);
        modelAndView.setViewName("user_all");
        return modelAndView;
    }
}

package am.online.banking.system.controller;

import am.online.banking.system.bean.dto.CreateDepositDto;
import am.online.banking.system.bean.dto.CreateWithdrawDto;
import am.online.banking.system.bean.dto.EditDepositWithdrawDto;
import am.online.banking.system.bean.entity.CardAccount;
import am.online.banking.system.bean.entity.CardTransaction;
import am.online.banking.system.bean.entity.PassiveTransaction;
import am.online.banking.system.bean.entity.User;
import am.online.banking.system.mapper.CardTransactionMapper;
import am.online.banking.system.mapper.PassiveTransactionMapper;
import am.online.banking.system.service.CardTransactionService;
import am.online.banking.system.service.PassiveTransactionService;
import am.online.banking.system.util.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/transaction")
public class TransactionController {

    private CardTransactionMapper cardTransactionMapper;
    private PassiveTransactionMapper passiveTransactionMapper;
    private CardTransactionService cardTransactionService;
    private PassiveTransactionService passiveTransactionService;
    private CurrentUser currentUser;

    @Autowired
    public TransactionController(CardTransactionMapper cardTransactionMapper, PassiveTransactionMapper passiveTransactionMapper,
                                 CardTransactionService cardTransactionService, PassiveTransactionService passiveTransactionService,
                                 CurrentUser currentUser) {
        this.cardTransactionMapper = cardTransactionMapper;
        this.passiveTransactionMapper = passiveTransactionMapper;
        this.cardTransactionService = cardTransactionService;
        this.passiveTransactionService = passiveTransactionService;
        this.currentUser = currentUser;
    }

    @GetMapping("/deposit")
    public ModelAndView showDepositForm() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("createDepositDto", new CreateDepositDto());
        modelAndView.setViewName("deposit_create");
        return modelAndView;
    }

    @PostMapping("/deposit/save")
    public ModelAndView saveDeposit(@Valid @ModelAttribute("depositDto") CreateDepositDto createDepositDto, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        User user = currentUser.getCurrentUser();
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("deposit_create");
            return modelAndView;
        }
        if (createDepositDto.getType().equals("Card")) {
            CardTransaction cardTransaction = cardTransactionMapper.map(createDepositDto, CardTransaction.class);
            CardAccount cardAccount = user.getCardAccount();
            cardTransactionService.deposit(cardTransaction, cardAccount);
        } else {
            PassiveTransaction passiveTransaction = passiveTransactionMapper.map(createDepositDto, PassiveTransaction.class);
            passiveTransactionService.deposit(passiveTransaction);
        }
//        modelAndView.setViewName("redirect:/user/home");
        modelAndView.addObject("firstName", user.getFirstName());
        modelAndView.addObject("tab", " ");
        modelAndView.addObject("lastName", user.getLastName());
        modelAndView.addObject("cardAccount", user.getCardAccount());
        modelAndView.addObject("passiveAccount", user.getPassiveAccount());
//        modelAndView.addObject("process", "SUCCESS");
//        modelAndView.addObject("pw_success", "Well done! The transaction is in the pending phase.");
        modelAndView.setViewName("user_home");
        return modelAndView;
    }

    @GetMapping("/depositOrWithdraw/edit/{id}")
    public ModelAndView showDepositEditForm(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView();
        User user = currentUser.getCurrentUser();
        String username = user.getUsername();
        CardTransaction cardTransaction = cardTransactionService.findCardTransactionById(id);

        if (!cardTransaction.getStatus().equals("Pending")) {
            modelAndView.addObject("cardAccount", user.getCardAccount());
            modelAndView.addObject("cardTransactionList", cardTransactionService.findCardTransactionList(username));
            modelAndView.addObject("process", "ERROR");
            modelAndView.addObject("pw_error", "Error:  Transaction already " + cardTransaction.getStatus() + " !");
            modelAndView.setViewName("card_account");
        } else if (cardTransaction.getDescription().equals("Deposit to Card Account")) {
            EditDepositWithdrawDto editDepositWithdrawDto = cardTransactionMapper.map(cardTransaction, EditDepositWithdrawDto.class);
            modelAndView.addObject("editDepositWithdrawDto", editDepositWithdrawDto);
            modelAndView.setViewName("deposit_edit");
        } else if (cardTransaction.getDescription().equals("Withdraw from Card Account")) {
            EditDepositWithdrawDto editDepositWithdrawDto = cardTransactionMapper.map(cardTransaction, EditDepositWithdrawDto.class);
            modelAndView.addObject("editDepositWithdrawDto", editDepositWithdrawDto);
            modelAndView.setViewName("withdraw_edit");
        }
        return modelAndView;
    }

    @PostMapping("/deposit/edit/save")
    public ModelAndView saveDepositEdit(@Valid @ModelAttribute("editDepositWithdrawDto") EditDepositWithdrawDto
                                                editDepositWithdrawDto, BindingResult bindingResult) {

        ModelAndView modelAndView = new ModelAndView();
        CardTransaction cardTransaction = cardTransactionMapper.map(editDepositWithdrawDto, CardTransaction.class);
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("deposit_edit");
            return modelAndView;
        }
        if (editDepositWithdrawDto.getType().equals("Card")) {
            cardTransactionService.depositEdit(cardTransaction);
        } else {
            PassiveTransaction passiveTransaction = passiveTransactionMapper.map(editDepositWithdrawDto, PassiveTransaction.class);
            passiveTransactionService.depositEdit(passiveTransaction);
        }
//        modelAndView.setViewName("redirect:/card/account/details");
        modelAndView.addObject("process", "SUCCESS");
        modelAndView.addObject("pw_success", "Well done! Transaction successfully updated.");
        User user = currentUser.getCurrentUser();
        modelAndView.addObject("cardAccount", user.getCardAccount());
        modelAndView.addObject("cardTransactionList", cardTransactionService.findCardTransactionList(user.getUsername()));
        modelAndView.setViewName("card_account");
        return modelAndView;
    }

        @PostMapping("/withdraw/edit/save")
    public ModelAndView saveWithdrawEdit(@Valid @ModelAttribute("editDepositWithdrawDto") EditDepositWithdrawDto editDepositWithdrawDto,
                                        BindingResult bindingResult) {

        ModelAndView modelAndView = new ModelAndView();
        CardTransaction cardTransaction = cardTransactionMapper.map(editDepositWithdrawDto, CardTransaction.class);
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("withdraw_edit");
            return modelAndView;
        }
        if (editDepositWithdrawDto.getType().equals("Card")) {
            cardTransactionService.withdrawEdit(cardTransaction);
        } else {
            PassiveTransaction passiveTransaction = passiveTransactionMapper.map(editDepositWithdrawDto, PassiveTransaction.class);
            passiveTransactionService.withdrawEdit(passiveTransaction);
        }
//        modelAndView.setViewName("redirect:/card/account/details");
        modelAndView.addObject("process", "SUCCESS");
        modelAndView.addObject("pw_success", "Well done! Transaction successfully updated.");
        User user = currentUser.getCurrentUser();
        modelAndView.addObject("cardAccount", user.getCardAccount());
        modelAndView.addObject("cardTransactionList", cardTransactionService.findCardTransactionList(user.getUsername()));
        modelAndView.setViewName("card_account");
        return modelAndView;
    }



    @RequestMapping(value = "/delete/{id}", method = {RequestMethod.DELETE, RequestMethod.GET})
    public ModelAndView cancelTransaction(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView();
        User user = currentUser.getCurrentUser();
        String username = user.getUsername();
        CardTransaction cardTransaction = cardTransactionService.findCardTransactionById(id);
        if (cardTransaction.getStatus().equals("Pending")) {
            cardTransactionService.delete(id);
            modelAndView.addObject("cardAccount", user.getCardAccount());
            modelAndView.addObject("cardTransactionList", cardTransactionService.findCardTransactionList(username));
            modelAndView.addObject("process", "SUCCESS");
            modelAndView.addObject("pw_success", "Well done! Transaction successfully canceled.");
            modelAndView.setViewName("card_account");
        } else {
            modelAndView.addObject("cardAccount", user.getCardAccount());
            modelAndView.addObject("cardTransactionList", cardTransactionService.findCardTransactionList(username));
            modelAndView.addObject("process", "ERROR");
            modelAndView.addObject("pw_error", "Error: Transaction already Accepted!");
            modelAndView.setViewName("card_account");
        }
        return modelAndView;
    }

    @GetMapping("/withdraw")
    public ModelAndView showWithdrawForm() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("createWithdrawDto", new CreateWithdrawDto());
        modelAndView.setViewName("withdraw_create");
        return modelAndView;
    }

    @PostMapping("/withdraw/save")
    public ModelAndView saveWithdraw(@Valid @ModelAttribute("createWithdrawDto") CreateWithdrawDto
                                             createWithdrawDto,
                                     BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        User user = currentUser.getCurrentUser();
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("withdraw_create");
            return modelAndView;
        }
        if (createWithdrawDto.getType().equals("Card")) {
            CardTransaction cardTransaction = cardTransactionMapper.map(createWithdrawDto, CardTransaction.class);
            CardAccount cardAccount = user.getCardAccount();
            cardTransactionService.withdraw(cardTransaction, cardAccount);
        } else {
            PassiveTransaction passiveTransaction = passiveTransactionMapper.map(createWithdrawDto, PassiveTransaction.class);
            passiveTransactionService.withdraw(passiveTransaction);
        }
//        modelAndView.setViewName("redirect:/user/home");
        modelAndView.addObject("firstName", user.getFirstName());
        modelAndView.addObject("tab", " ");
        modelAndView.addObject("lastName", user.getLastName());
        modelAndView.addObject("cardAccount", user.getCardAccount());
        modelAndView.addObject("passiveAccount", user.getPassiveAccount());
//        modelAndView.addObject("process", "SUCCESS");
//        modelAndView.addObject("pw_success", "Well done! The transaction is in the pending phase.");
        modelAndView.setViewName("user_home");
        return modelAndView;
    }
}

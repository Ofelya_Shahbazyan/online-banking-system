package am.online.banking.system.controller;

import am.online.banking.system.bean.entity.User;
import am.online.banking.system.service.PassiveTransactionService;
import am.online.banking.system.service.UserService;
import am.online.banking.system.util.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/passive/account")
public class PassiveAccountController {

    private UserService userService;
    private PassiveTransactionService passiveTransactionService;
    private CurrentUser currentUser;

    @Autowired
    public PassiveAccountController(UserService userService, PassiveTransactionService passiveTransactionService, CurrentUser currentUser) {
        this.userService = userService;
        this.passiveTransactionService = passiveTransactionService;
        this.currentUser = currentUser;
    }

    @GetMapping("/details")
    public ModelAndView cardAccount() {
        ModelAndView modelAndView = new ModelAndView();
        User user = currentUser.getCurrentUser();
        modelAndView.addObject("passiveAccount", user.getPassiveAccount());
        modelAndView.addObject("passiveTransactionList", passiveTransactionService.findPassiveTransactionList(user.getUsername()));
        modelAndView.setViewName("passive_account");
        return modelAndView;
    }
}





























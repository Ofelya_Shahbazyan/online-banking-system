package am.online.banking.system.controller;

import am.online.banking.system.bean.dto.CardTransactionDto;
import am.online.banking.system.bean.entity.CardTransaction;
import am.online.banking.system.mapper.CardTransactionMapper;
import am.online.banking.system.service.CardTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/transaction/resource")
public class TransactionResourceController {

    private CardTransactionService cardTransactionService;
    private CardTransactionMapper cardTransactionMapper;

    @Autowired
    public TransactionResourceController(CardTransactionService cardTransactionService, CardTransactionMapper cardTransactionMapper) {
        this.cardTransactionService = cardTransactionService;
        this.cardTransactionMapper = cardTransactionMapper;
    }

    @GetMapping("/user/card/transaction")
    public ModelAndView getCardTransaction() {
        ModelAndView modelAndView = new ModelAndView();
        List<CardTransaction> cardTransactionList = cardTransactionService.findAllCardTransaction();
        modelAndView.addObject("cardTransactionList", cardTransactionList);
        modelAndView.setViewName("card_transaction_all");
        return modelAndView;
    }

    @GetMapping("/accept/reject/{id}")
    public ModelAndView acceptOrReject(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView();
        CardTransaction cardTransaction = cardTransactionService.findCardTransactionById(id);
//        CardTransactionDto cardTransactionDto = cardTransactionMapper.map(cardTransaction, CardTransactionDto.class);
        if (!cardTransaction.getStatus().equals("Pending")) {
            modelAndView.addObject("process", "ERROR");
            modelAndView.addObject("pw_error", "Error:  Transaction already " + cardTransaction.getStatus() + " !");
            List<CardTransaction> cardTransactionList = cardTransactionService.findAllCardTransaction();
            modelAndView.addObject("cardTransactionList", cardTransactionList);
            modelAndView.setViewName("card_transaction_all");
        } else {
            modelAndView.addObject("cardTransactionDto", cardTransaction);
            modelAndView.setViewName("accept_reject");
        }
        return modelAndView;
    }

    @PostMapping("/accepted/rejected")
    public ModelAndView acceptedOrRejected(@Valid @ModelAttribute("cardTransactionDto") CardTransaction cardTransaction,
                                           BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("accept_reject");
            return modelAndView;
        }
//        CardTransaction cardTransaction = cardTransactionMapper.map(cardTransactionDto, CardTransaction.class);
        String status = cardTransaction.getStatus();
        String description = cardTransaction.getDescription();
        if (status.equals("Accepted") && description.equals("Deposit to Card Account")) {
            cardTransactionService.acceptDeposit(cardTransaction);
        } else  if (status.equals("Accepted") && description.equals("Withdraw from Card Account")) {
            cardTransactionService.acceptWithdraw(cardTransaction);
        } else {
            cardTransactionService.reject(cardTransaction);
        }
//        modelAndView.setViewName("redirect:/transaction/resource/user/card/transaction");
//        modelAndView.addObject("process", "SUCCESS");
//        modelAndView.addObject("pw_success", "Well done! Transaction successfully " + cardTransaction.getStatus() + " !");
        List<CardTransaction> cardTransactionList = cardTransactionService.findAllCardTransaction();
        modelAndView.addObject("cardTransactionList", cardTransactionList);
        modelAndView.setViewName("card_transaction_all");
        return modelAndView;
    }

}

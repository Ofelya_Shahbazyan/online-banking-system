package am.online.banking.system.controller;

import am.online.banking.system.bean.entity.User;
import am.online.banking.system.service.CardTransactionService;
import am.online.banking.system.service.UserService;
import am.online.banking.system.util.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/card/account")
public class CardAccountController {

    private UserService userService;
    private CardTransactionService cardTransactionService;
    private CurrentUser currentUser;

    @Autowired
    public CardAccountController(UserService userService, CardTransactionService cardTransactionService, CurrentUser currentUser) {
        this.userService = userService;
        this.cardTransactionService = cardTransactionService;
        this.currentUser = currentUser;
    }

    @GetMapping("/details")
    public ModelAndView cardAccount() {
        ModelAndView modelAndView = new ModelAndView();
        User user = currentUser.getCurrentUser();
        modelAndView.addObject("cardAccount", user.getCardAccount());
        modelAndView.addObject("cardTransactionList", cardTransactionService.findCardTransactionList(user.getUsername()));
        modelAndView.setViewName("card_account");
        return modelAndView;
    }
}





























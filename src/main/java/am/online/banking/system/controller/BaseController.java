package am.online.banking.system.controller;

import am.online.banking.system.bean.dto.SignInDto;
import am.online.banking.system.bean.entity.Role;
import am.online.banking.system.bean.entity.User;
import am.online.banking.system.service.UserService;
import am.online.banking.system.util.CurrentUser;
import am.online.banking.system.util.ResponseStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

@Controller
public class BaseController {

    private MessageSource messageSource;
    private UserService userService;
    private BCryptPasswordEncoder passwordEncoder;
    private CurrentUser currentUser;

    @Autowired
    public BaseController(MessageSource messageSource, UserService userService, BCryptPasswordEncoder passwordEncoder, CurrentUser currentUser) {
        this.messageSource = messageSource;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.currentUser = currentUser;
    }

    @GetMapping("/")
    public String index() {
        return "redirect:/index";
    }

    @GetMapping("/index")
    public String showIndexPage() {
        return "index";
    }

    @GetMapping("/login")
    public ModelAndView showLoginPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", new SignInDto());
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @PostMapping("/signed/successfully")
    public String signedSuccessfully() {
        return "redirect:/home";
    }


//    @PostMapping("/login")
//    public ModelAndView loginForm(@Valid @ModelAttribute("user") SignInDto signInDto, BindingResult bindingResult) {
//        ModelAndView modelAndView = new ModelAndView();
//
//        if (bindingResult.hasErrors()) {
//            modelAndView.addObject("user", signInDto);
//            modelAndView.setViewName("login");
//            return modelAndView;
//        }
//        modelAndView.setViewName("redirect:/home");
//        return modelAndView;
//    }

    @GetMapping("/home")
    public ModelAndView homePage() {
        ModelAndView modelAndView = new ModelAndView();
        User user = currentUser.getCurrentUser();
        List<Role> role = user.getRoles();
        if (role.stream().map(Role::getRole).anyMatch("USER"::equals)) {
            modelAndView.setViewName("redirect:/user/home");
        } else {
            modelAndView.setViewName("redirect:/admin/home");
        }
        return modelAndView;
    }

    @RequestMapping("/login/error")
    public ModelAndView loginError(@RequestParam(value = "error", required = false) final int error, Locale locale) {
        ModelAndView modelAndView = new ModelAndView();
        ResponseStatus errorStatus = ResponseStatus.valueOf(error);
        String errorMessage;
        String messageKey;

        if (errorStatus != null) {
            messageKey = errorStatus.getMessageKey();
        } else {
            messageKey = "user.auth.failed";
        }
        errorMessage = messageSource.getMessage(messageKey, null, locale);
        modelAndView.addObject("errorMessage", errorMessage);
        modelAndView.setViewName("error");
        return modelAndView;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(Model model) {
        return "redirect:/";
    }

//    @RequestMapping(value = REQUEST_ERROR, method = RequestMethod.GET)
//    public String error(HttpServletRequest request, Model model) {
//        model.addAttribute(ERROR_CODE, ERROR + request.getAttribute("javax.servlet.error.status_code"));
//        StringBuilder errorMessage = new StringBuilder();
//        errorMessage.append("<ul>");
//
//        errorMessage.append("</ul>");
//        model.addAttribute(ERROR_MESSAGE, errorMessage.toString());
//        return PAGE_ERROR;
//    }

}

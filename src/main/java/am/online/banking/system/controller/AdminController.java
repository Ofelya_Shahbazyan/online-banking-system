package am.online.banking.system.controller;

import am.online.banking.system.bean.entity.User;
import am.online.banking.system.util.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private CurrentUser currentUser;

    @Autowired
    public AdminController(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }

    @GetMapping("/home")
    public ModelAndView showAdminPage() {
        ModelAndView modelAndView = new ModelAndView();
        User user = currentUser.getCurrentUser();
        modelAndView.addObject("firstName", user.getFirstName());
        modelAndView.addObject("tab", " ");
        modelAndView.addObject("lastName", user.getLastName());
        modelAndView.setViewName("admin_home");
        return modelAndView;
    }
}

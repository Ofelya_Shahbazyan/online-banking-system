//package am.online.banking.system.validation;
//
//import javax.validation.Constraint;
//import javax.validation.Payload;
//import java.lang.annotation.*;
//
//@Documented
//@Constraint(validatedBy = UsernamePasswordValidator.class)
//@Target( { ElementType.METHOD, ElementType.FIELD })
//@Retention(RetentionPolicy.RUNTIME)
//public @interface UsernamePasswordValidation {
//
//    String fieldName();
//
//    String message() default "*Username or password invalid!";
//    Class<?>[] groups() default {};
//    Class<? extends Payload>[] payload() default {};
//}

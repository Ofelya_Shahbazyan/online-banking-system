package am.online.banking.system.validation;

import am.online.banking.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class IsUniqueValidator implements ConstraintValidator<IsUniqueValidation, String> {

    private String fieldName;
    private final UserService userService;

    @Autowired
    public IsUniqueValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void initialize(IsUniqueValidation constraintAnnotation) {
        fieldName = constraintAnnotation.fieldName();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        switch (fieldName) {
            case "Username":
                return userService.isUsernameUnique(value);
            case "Email":
                return userService.isEmailUnique(value);
            case "Phone":
                return userService.isPhoneUnique(value);
        }
        return false;
    }
}

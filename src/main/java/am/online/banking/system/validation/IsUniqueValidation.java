package am.online.banking.system.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Constraint(validatedBy = IsUniqueValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IsUniqueValidation {

    String fieldName();

    String message() default "*There is a user with this credential!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

//package am.online.banking.system.validation;
//
//import am.online.banking.system.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import javax.validation.ConstraintValidator;
//import javax.validation.ConstraintValidatorContext;
//
//@SuppressWarnings("FieldCanBeLocal")
//class UsernamePasswordValidator implements ConstraintValidator<UsernamePasswordValidation, String> {
//
//    private String fieldName;
//    private final UserService userService;
//
//    private static String username;
//    private static String password;
//    private static boolean isUsernameCorrect;
//    private static boolean isPasswordCorrect;
//
//    @Autowired
//    public UsernamePasswordValidator(UserService userService) {
//        this.userService = userService;
//    }
//
//    @Override
//    public void initialize(UsernamePasswordValidation constraintAnnotation) {
//        fieldName = constraintAnnotation.fieldName();
//    }
//
//    @Override
//    public boolean isValid(String value, ConstraintValidatorContext context) {
//        if (fieldName.equals("Username")) {
//            username = value;
//            isUsernameCorrect = !userService.isUsernameUnique(username);
//            return true;
//        } else if (fieldName.equals("Password")) {
//            password = value;
//            isPasswordCorrect = userService.isUsernameAndPasswordCorrect(username, password);
//        }
//
//        username = null;
//        password = null;
//
//        return isUsernameCorrect && isPasswordCorrect;
//    }
//}

package am.online.banking.system.repository;

import am.online.banking.system.bean.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByUsername(String username);

    User findUserByUsernameAndPassword(String username, String password);

    User findUserByEmail(String email);

    User findUserByPhone(String phone);

    User findUserByCardAccountId(Long id);
}

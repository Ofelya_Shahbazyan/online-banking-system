package am.online.banking.system.repository;

import am.online.banking.system.bean.entity.Notification;
import am.online.banking.system.bean.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    @Query("Select u from Notification u where u.user= :userId ORDER BY u.createdDate  ")
    List<Notification> getNotificationByUser(@Param("userId") User userId);
}

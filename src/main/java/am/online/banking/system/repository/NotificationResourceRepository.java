package am.online.banking.system.repository;

import am.online.banking.system.bean.entity.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface NotificationResourceRepository extends JpaRepository<Notification, Long> {
}

package am.online.banking.system.repository;

import am.online.banking.system.bean.entity.PassiveAccount;
import am.online.banking.system.bean.entity.PassiveTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface PassiveTransactionRepository extends JpaRepository<PassiveTransaction, Long> {

    List<PassiveTransaction> findAllByPassiveAccount(PassiveAccount passiveAccount);
}

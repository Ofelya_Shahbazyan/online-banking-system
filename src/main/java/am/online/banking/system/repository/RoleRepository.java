package am.online.banking.system.repository;

import am.online.banking.system.bean.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface RoleRepository extends JpaRepository<Role,Long> {

    Optional<Role> findUserByRole(String role);

}

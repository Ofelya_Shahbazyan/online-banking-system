package am.online.banking.system.repository;

import am.online.banking.system.bean.entity.CardAccount;
import am.online.banking.system.bean.entity.CardTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface CardAccountRepository extends JpaRepository<CardAccount, Long> {
    
    CardAccount findCardAccountById(Long id);

    CardAccount findCardAccountByAccountNumber(Long number);
}

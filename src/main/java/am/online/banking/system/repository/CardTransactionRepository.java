package am.online.banking.system.repository;

import am.online.banking.system.bean.entity.CardAccount;
import am.online.banking.system.bean.entity.CardTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface CardTransactionRepository extends JpaRepository<CardTransaction, Long> {

    List<CardTransaction> findAllByCardAccount(CardAccount cardAccount);

    CardTransaction findCardTransactionById(Long id);

}

package am.online.banking.system.util;

import am.online.banking.system.bean.entity.User;
import am.online.banking.system.security.CustomUserDetails;
import am.online.banking.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class CurrentUser {

    private UserService userService;

    @Autowired
    public CurrentUser(UserService userService) {
        this.userService = userService;
    }

    public User getCurrentUser() {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = customUserDetails.getUsername();
        User user = userService.findUserByUsername(username);
        return user;
    }
}

package am.online.banking.system.mapper;

import am.online.banking.system.bean.dto.CreateDepositDto;
import am.online.banking.system.bean.entity.CardTransaction;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingException;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class CardTransactionMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        String fieldName;
        try {
            fieldName = CreateDepositDto.class.getDeclaredField("type").getName();
        } catch (NoSuchFieldException ignored) {
            throw new MappingException("Ignored field dose not exist.");
        }
        factory.classMap(CreateDepositDto.class, CardTransaction.class).fieldAToB(fieldName, fieldName).byDefault().register();
    }
}


package am.online.banking.system.mapper;

import am.online.banking.system.bean.dto.NotificationDto;
import am.online.banking.system.bean.entity.Notification;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class NotificationMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {

        factory.classMap(Notification.class, NotificationDto.class)
                .byDefault()
                .register();
    }
}

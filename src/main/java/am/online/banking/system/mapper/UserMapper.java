package am.online.banking.system.mapper;

import am.online.banking.system.bean.dto.CreateUserDto;
import am.online.banking.system.bean.entity.User;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingException;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        String fieldName;
        try {
            fieldName = CreateUserDto.class.getDeclaredField("password").getName();
        } catch (NoSuchFieldException ignored) {
            throw new MappingException("Ignored field dose not exist.");
        }

        factory.classMap(CreateUserDto.class, User.class).fieldAToB(fieldName, fieldName).byDefault().register();
    }
}


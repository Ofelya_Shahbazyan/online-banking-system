package am.online.banking.system.exception;



public class OperationFailedException extends RuntimeException {
    private String message;

    public OperationFailedException(final String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

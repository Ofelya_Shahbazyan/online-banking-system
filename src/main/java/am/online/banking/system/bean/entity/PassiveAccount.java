package am.online.banking.system.bean.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
public class PassiveAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private Long id;

    @Column(name = "account_number")
    private Long accountNumber;

    @Column(name = "account_balance")
    private BigDecimal accountBalance;

    @OneToMany(mappedBy = "passiveAccount")
    List<PassiveTransaction> passiveTransactionList;

    public PassiveAccount() {
    }

    public PassiveAccount(Long accountNumber, BigDecimal accountBalance) {
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    public List<PassiveTransaction> getPassiveTransactionList() {
        return passiveTransactionList;
    }

    public void setPassiveTransactionList(List<PassiveTransaction> passiveTransactionList) {
        this.passiveTransactionList = passiveTransactionList;
    }
}



package am.online.banking.system.bean.dto;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;

public class CreateDepositDto implements Serializable {

    @NotBlank(message = "*Select your account!")
    private String type;

    @DecimalMin(value = "1.0", inclusive = false, message = "*Amount must be between 1 - 500 000 AMD")
    @DecimalMax(value = "500000.0", inclusive = false, message = "*Amount must be between 1 - 500 000 AMD")
//    @Digits(integer = 6, fraction = 2)
    private BigDecimal amount;

    public CreateDepositDto() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}

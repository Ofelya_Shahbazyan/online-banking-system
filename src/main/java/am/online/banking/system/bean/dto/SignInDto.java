package am.online.banking.system.bean.dto;

//import am.online.banking.system.validation.UsernamePasswordValidation;

import java.io.Serializable;

public class SignInDto implements Serializable {


//    @UsernamePasswordValidation(fieldName = "Username")
    private String username;

//    @UsernamePasswordValidation(fieldName = "Password")
    private String password;

    public SignInDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

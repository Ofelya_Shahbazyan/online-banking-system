package am.online.banking.system.bean.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
public class CardAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private Long id;

    @Column(name = "account_number")
    private Long accountNumber;

    @Column(name = "account_balance")
    private BigDecimal accountBalance;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "user_id")
//    private User user;

    @OneToMany(mappedBy = "cardAccount")
    List<CardTransaction> cardTransactionList;

    public CardAccount() {
    }

    public CardAccount(Long accountNumber, BigDecimal accountBalance, List<CardTransaction> cardTransactionList) {
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
        this.cardTransactionList = cardTransactionList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }


    public List<CardTransaction> getCardTransactionList() {
        return cardTransactionList;
    }

    public void setCardTransactionList(List<CardTransaction> cardTransactionList) {
        this.cardTransactionList = cardTransactionList;
    }
}

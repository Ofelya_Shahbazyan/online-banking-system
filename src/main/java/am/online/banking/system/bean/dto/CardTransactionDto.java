package am.online.banking.system.bean.dto;

import am.online.banking.system.bean.entity.CardAccount;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CardTransactionDto implements Serializable {

    private Long id;

    private String type;

    private String description;

    private String senderAccountNumber;

    private String receiverAccountNumber;

    private Date date;

    private BigDecimal amount;

    @NotBlank(message = "*Select status!")
    private String status;

    private BigDecimal availableBalance;

    private CardAccount cardAccount;

    public CardTransactionDto() {
    }

    public CardTransactionDto(String type, String description, String senderAccountNumber, String receiverAccountNumber,
                              Date date, BigDecimal amount, String status, BigDecimal availableBalance, CardAccount cardAccount) {
        this.type = type;
        this.description = description;
        this.senderAccountNumber = senderAccountNumber;
        this.receiverAccountNumber = receiverAccountNumber;
        this.date = date;
        this.amount = amount;
        this.status = status;
        this.availableBalance = availableBalance;
        this.cardAccount = cardAccount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSenderAccountNumber() {
        return senderAccountNumber;
    }

    public void setSenderAccountNumber(String senderAccountNumber) {
        this.senderAccountNumber = senderAccountNumber;
    }

    public String getReceiverAccountNumber() {
        return receiverAccountNumber;
    }

    public void setReceiverAccountNumber(String receiverAccountNumber) {
        this.receiverAccountNumber = receiverAccountNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public CardAccount getCardAccount() {
        return cardAccount;
    }

    public void setCardAccount(CardAccount cardAccount) {
        this.cardAccount = cardAccount;
    }
}



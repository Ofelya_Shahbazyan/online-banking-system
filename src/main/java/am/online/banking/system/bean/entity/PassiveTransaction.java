package am.online.banking.system.bean.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class PassiveTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private Long id;

    @Column(name = "account_type")
    private String type;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "sender_account_number")
    private String senderAccountNumber;

    @NotNull
    @Column(name = "receiver_account_number")
    private String receiverAccountNumber;

    @NotNull
    @Column(name = "date")
    private Date date;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "status")
    private String status;

    @Column(name = "available_balance")
    private BigDecimal availableBalance;

    @ManyToOne
    @JoinColumn(name = "passive_account_id")
    private PassiveAccount passiveAccount;

    public PassiveTransaction() {
    }

    public PassiveTransaction(String type, String description, String senderAccountNumber, String receiverAccountNumber,
                              Date date, BigDecimal amount, String status, BigDecimal availableBalance,
                              PassiveAccount passiveAccount) {
        this.type = type;
        this.description = description;
        this.senderAccountNumber = senderAccountNumber;
        this.receiverAccountNumber = receiverAccountNumber;
        this.date = date;
        this.amount = amount;
        this.status = status;
        this.availableBalance = availableBalance;
        this.passiveAccount = passiveAccount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSenderAccountNumber() {
        return senderAccountNumber;
    }

    public void setSenderAccountNumber(String senderAccountNumber) {
        this.senderAccountNumber = senderAccountNumber;
    }

    public String getReceiverAccountNumber() {
        return receiverAccountNumber;
    }

    public void setReceiverAccountNumber(String receiverAccountNumber) {
        this.receiverAccountNumber = receiverAccountNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public PassiveAccount getPassiveAccount() {
        return passiveAccount;
    }

    public void setPassiveAccount(PassiveAccount passiveAccount) {
        this.passiveAccount = passiveAccount;
    }
}


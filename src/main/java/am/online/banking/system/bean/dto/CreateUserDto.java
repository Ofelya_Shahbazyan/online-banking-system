package am.online.banking.system.bean.dto;

import am.online.banking.system.validation.EmailValidation;
import am.online.banking.system.validation.IsUniqueValidation;
import am.online.banking.system.validation.PasswordValidation;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class CreateUserDto implements Serializable {

    @Size(min = 2, max = 12, message = "*Name characters size must be between 2 and 12")
    private String firstName;

    private String lastName;

    @EmailValidation
    @IsUniqueValidation(fieldName = "Email")
    private String email;

    @PasswordValidation
    private String password;

    @NotEmpty
    private String matchingPassword;

    @IsUniqueValidation(fieldName = "Username")
    @Size(min = 5, max = 12, message = "*Username size should be between 5 and 12 characters.")
    private String username;

    @IsUniqueValidation(fieldName = "Phone")
    private String phone;

    public CreateUserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

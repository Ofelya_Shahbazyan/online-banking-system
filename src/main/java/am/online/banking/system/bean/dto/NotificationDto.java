package am.online.banking.system.bean.dto;

import java.io.Serializable;
import java.util.Date;

public class NotificationDto implements Serializable {

    private Date createdDate;
    private String message;

    public NotificationDto() {
    }

    public NotificationDto( Date createdDate, String message) {
        this.createdDate = createdDate;
        this.message = message;
    }


    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

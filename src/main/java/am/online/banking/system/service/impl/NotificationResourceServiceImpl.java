package am.online.banking.system.service.impl;

import am.online.banking.system.bean.dto.NotificationDto;
import am.online.banking.system.bean.entity.Notification;
import am.online.banking.system.mapper.NotificationMapper;
import am.online.banking.system.repository.NotificationResourceRepository;
import am.online.banking.system.service.NotificationResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class NotificationResourceServiceImpl implements NotificationResourceService {

    private NotificationResourceRepository notificationResourceRepository;
    private NotificationMapper notificationMapper;

    @Autowired
    public NotificationResourceServiceImpl(NotificationResourceRepository notificationResourceRepository,
                                           NotificationMapper notificationMapper) {
        this.notificationResourceRepository = notificationResourceRepository;
        this.notificationMapper = notificationMapper;
    }

    @Override
    public List<NotificationDto> getAllNotifications() {
        List<Notification> notifications = notificationResourceRepository.findAll();
        return notificationMapper.mapAsList(notifications, NotificationDto.class);
    }
}

package am.online.banking.system.service.impl;

import am.online.banking.system.bean.entity.Role;
import am.online.banking.system.bean.entity.User;
import am.online.banking.system.repository.RoleRepository;
import am.online.banking.system.repository.UserRepository;
import am.online.banking.system.service.CardAccountService;
import am.online.banking.system.service.PassiveAccountService;
import am.online.banking.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private RoleRepository roleRepository;
    private CardAccountService cardAccountService;
    private PassiveAccountService passiveAccountService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder, RoleRepository roleRepository, CardAccountService cardAccountService, PassiveAccountService passiveAccountService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.cardAccountService = cardAccountService;
        this.passiveAccountService = passiveAccountService;
    }

    @Override
    public boolean isUsernameUnique(String username) {
        return userRepository.findUserByUsername(username) == null;
    }

//    @Override
//    public boolean isUsernameAndPasswordCorrect(String username, String password) {
//        return userRepository.findUserByUsernameAndPassword(username, password) != null;
//    }

    @Override
    public boolean isEmailUnique(String email) {
        return userRepository.findUserByEmail(email) == null;
    }

    @Override
    public boolean isPhoneUnique(String phone) {
        return userRepository.findUserByPhone(phone) == null;
    }

    @Override
    public void createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Optional<Role> role = roleRepository.findUserByRole("USER");
        if (role.isPresent()) {
            List<Role> roleNameSet = Collections.singletonList(role.get());
            user.setRoles(roleNameSet);
        }
        user.setCardAccount(cardAccountService.createCardAccount());
        user.setPassiveAccount(passiveAccountService.createPassiveAccount());
        user.setEnabled(true);
        user.setAccountNonLocked(true);
        user.setAccountNonExpired(true);
        user.setCredentialsNonExpired(true);
        userRepository.save(user);
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }
}

package am.online.banking.system.service;

import am.online.banking.system.bean.entity.User;

import java.util.List;

public interface UserService {

    boolean isUsernameUnique(String username);

//    boolean isUsernameAndPasswordCorrect(String username, String password);

    boolean isEmailUnique(String value);

    boolean isPhoneUnique(String value);

    void createUser(User user);

    User findUserByUsername(String username);

    List<User> findAllUsers();
}

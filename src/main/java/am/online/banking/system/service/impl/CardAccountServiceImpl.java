package am.online.banking.system.service.impl;

import am.online.banking.system.bean.entity.CardAccount;
import am.online.banking.system.repository.CardAccountRepository;
import am.online.banking.system.service.CardAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.UUID;

@Service
@Transactional
public class CardAccountServiceImpl implements CardAccountService {

//    private static Long nextAccountNumber = 1000000000000001L;


    private CardAccountRepository cardAccountRepository;

    @Autowired
    public CardAccountServiceImpl(CardAccountRepository cardAccountRepository) {
        this.cardAccountRepository = cardAccountRepository;
    }

    @Override
    public CardAccount createCardAccount() {
        CardAccount cardAccount = new CardAccount();
        cardAccount.setAccountBalance(new BigDecimal(0.0));
        cardAccount.setAccountNumber(accountGen());
        cardAccountRepository.save(cardAccount);
        return cardAccount;
    }

    //    private Long accountGen() {
////        Long newNextAccountNumber = ++nextAccountNumber;
////        nextAccountNumber = newNextAccountNumber;
////        return newNextAccountNumber;
//        Long accountNumber;
//        do {
//            accountNumber = ++nextAccountNumber;
//        } while (isAccountNumberExists(accountNumber));
//        return accountNumber;
//
////        return ++nextAccountNumber;
//    }
//

    private Long accountGen() {
        Long getNumber;
        String number;
        do {
            String uniqueKey = String.format("%040d", new BigInteger(UUID.randomUUID().toString().replace("-", ""),16));
            number = uniqueKey.replace("0","7");
            getNumber = new Long(number.substring(0, 16));
        } while (isAccountNumberExists(getNumber));
        return getNumber;
    }

    private boolean isAccountNumberExists(Long number) {
        return cardAccountRepository.findCardAccountByAccountNumber(number) != null;
    }

}

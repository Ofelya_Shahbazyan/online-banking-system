package am.online.banking.system.service.impl;

import am.online.banking.system.bean.entity.PassiveAccount;
import am.online.banking.system.repository.PassiveAccountRepository;
import am.online.banking.system.service.PassiveAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.UUID;

@Service
@Transactional
public class PassiveAccountServiceImpl implements PassiveAccountService {

//    private static Long nextAccountNumber = 7000000000000001L;


    private PassiveAccountRepository passiveAccountRepository;

    @Autowired
    public PassiveAccountServiceImpl(PassiveAccountRepository passiveAccountRepository) {
        this.passiveAccountRepository = passiveAccountRepository;
    }

    @Override
    public PassiveAccount createPassiveAccount() {
        PassiveAccount passiveAccount = new PassiveAccount();
        passiveAccount.setAccountBalance(new BigDecimal(0.0));
        passiveAccount.setAccountNumber(accountGen());
        passiveAccountRepository.save(passiveAccount);
        return passiveAccount;
    }

    //    private Long accountGen() {
////        Long newNextAccountNumber = ++nextAccountNumber;
////        nextAccountNumber = newNextAccountNumber;
////        return newNextAccountNumber;
//        Long accountNumber;
//        do {
//            accountNumber = ++nextAccountNumber;
//        } while (isAccountNumberExists(accountNumber));
//        return accountNumber;
//
////        return ++nextAccountNumber;
//    }
//

    private Long accountGen() {
        Long getNumber;
        String number;
        do {
            String uniqueKey = String.format("%040d", new BigInteger(UUID.randomUUID().toString().replace("-", ""),16));
            number = uniqueKey.replace("0","7");
            getNumber = new Long(number.substring(0, 16));
        } while (isAccountNumberExists(getNumber));
        return getNumber;
    }

    private boolean isAccountNumberExists(Long number) {
        return passiveAccountRepository.findPassiveAccountByAccountNumber(number) != null;
    }
}

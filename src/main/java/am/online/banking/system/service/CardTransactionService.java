package am.online.banking.system.service;

import am.online.banking.system.bean.entity.CardAccount;
import am.online.banking.system.bean.entity.CardTransaction;

import java.util.List;

public interface CardTransactionService {

    List<CardTransaction> findCardTransactionList(String username);

    void deposit(CardTransaction cardTransaction, CardAccount cardAccount);

    void delete(Long id);

//    void balanceAfterCancel(CardAccount cardAccount, BigDecimal amount);

    CardTransaction findCardTransactionById(Long id);

    void depositEdit(CardTransaction cardTransaction);

    List<CardTransaction> findAllCardTransaction();

    void acceptDeposit(CardTransaction cardTransaction);

    void reject(CardTransaction cardTransaction);

    void withdraw(CardTransaction cardTransaction, CardAccount cardAccount) ;

    void acceptWithdraw(CardTransaction cardTransaction);

    void withdrawEdit(CardTransaction cardTransaction);
}

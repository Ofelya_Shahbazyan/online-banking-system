package am.online.banking.system.service;

import am.online.banking.system.bean.entity.CardAccount;

public interface CardAccountService {

    CardAccount createCardAccount();

}

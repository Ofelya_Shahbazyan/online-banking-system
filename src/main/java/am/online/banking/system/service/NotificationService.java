package am.online.banking.system.service;

import am.online.banking.system.bean.dto.NotificationDto;
import am.online.banking.system.bean.entity.Notification;

import java.util.List;

public interface NotificationService {

    void addNotification(Notification notification);

    List<NotificationDto> getNotificationByRecipient();

}

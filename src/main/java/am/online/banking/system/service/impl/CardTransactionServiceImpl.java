package am.online.banking.system.service.impl;

import am.online.banking.system.bean.entity.CardAccount;
import am.online.banking.system.bean.entity.CardTransaction;
import am.online.banking.system.bean.entity.Notification;
import am.online.banking.system.bean.entity.User;
import am.online.banking.system.exception.OperationFailedException;
import am.online.banking.system.repository.CardAccountRepository;
import am.online.banking.system.repository.CardTransactionRepository;
import am.online.banking.system.repository.UserRepository;
import am.online.banking.system.service.CardTransactionService;
import am.online.banking.system.service.NotificationService;
import am.online.banking.system.service.UserService;
import am.online.banking.system.util.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CardTransactionServiceImpl implements CardTransactionService {

    private UserService userService;
    private CardTransactionRepository cardTransactionRepository;
    private CardAccountRepository cardAccountRepository;
    private CurrentUser currentUser;
    private NotificationService notificationService;
    private UserRepository userRepository;

    @Autowired
    public CardTransactionServiceImpl(UserService userService, CardTransactionRepository cardTransactionRepository, CardAccountRepository cardAccountRepository, CurrentUser currentUser, NotificationService notificationService, UserRepository userRepository) {
        this.userService = userService;
        this.cardTransactionRepository = cardTransactionRepository;
        this.cardAccountRepository = cardAccountRepository;
        this.currentUser = currentUser;
        this.notificationService = notificationService;
        this.userRepository = userRepository;
    }

    @Override
    public CardTransaction findCardTransactionById(Long id) {
        return cardTransactionRepository.findCardTransactionById(id);
    }

    @Override
    public List<CardTransaction> findCardTransactionList(String username) {
        User user = userService.findUserByUsername(username);
        CardAccount cardAccount = user.getCardAccount();
        return cardTransactionRepository.findAllByCardAccount(cardAccount);
    }

    @Override
    public List<CardTransaction> findAllCardTransaction() {
        return cardTransactionRepository.findAll();
    }

    @Override
    public void deposit(CardTransaction cardTransaction, CardAccount cardAccount) {
        BigDecimal amount = cardTransaction.getAmount();

        CardTransaction newCardTransaction = new CardTransaction();
        newCardTransaction.setAmount(amount);
        newCardTransaction.setAvailableBalance(cardAccount.getAccountBalance());
        newCardTransaction.setDate(new Date());
        newCardTransaction.setDescription("Deposit to Card Account");
        newCardTransaction.setStatus("Pending");
        newCardTransaction.setCardAccount(cardAccount);
        newCardTransaction.setType("Card");
        newCardTransaction.setSenderAccountNumber("-");
        newCardTransaction.setReceiverAccountNumber("-");

        cardTransactionRepository.save(newCardTransaction);

        Notification notification = Notification.builder()
                .admin(userRepository.findById((long) 1).get())
                .user(currentUser.getCurrentUser())
                .message("Well done! The transaction is in the pending phase.")
                .build();

        notificationService.addNotification(notification);
    }

    @Override
    public void depositEdit(CardTransaction cardTransaction) {
        CardTransaction editedCardTransaction = cardTransactionRepository.findCardTransactionById(cardTransaction.getId());
        editedCardTransaction.setAmount(cardTransaction.getAmount());
//        editedCardTransaction.setType(cardTransaction.getType());
        cardTransactionRepository.save(editedCardTransaction);
    }

    @Override
    public void acceptDeposit(CardTransaction cardTransaction) {

        CardAccount cardAccount = cardTransaction.getCardAccount();
        BigDecimal amount = cardTransaction.getAmount();

        cardTransaction.setAvailableBalance(cardAccount.getAccountBalance().add(amount));
        cardTransaction.setStatus(cardTransaction.getStatus());
        cardTransactionRepository.save(cardTransaction);

        cardAccount.setAccountBalance(cardAccount.getAccountBalance().add(amount));
        cardAccountRepository.save(cardAccount);

        Notification notification = Notification.builder()
                .admin(userRepository.findById((long) 1).get())
                .user(userRepository.findUserByCardAccountId(cardAccount.getId()))
                .message("Well done! Transaction successfully  Accepted.")
                .build();

        notificationService.addNotification(notification);

    }

    @Override
    public void reject(CardTransaction cardTransaction) {

        CardAccount cardAccount = cardTransaction.getCardAccount();

        cardTransaction.setAvailableBalance(cardAccount.getAccountBalance());
        cardTransaction.setStatus(cardTransaction.getStatus());
        cardTransactionRepository.save(cardTransaction);


        CardAccount userCardAccount = cardAccountRepository.findCardAccountById(cardAccount.getId());
        cardAccount.setAccountBalance(userCardAccount.getAccountBalance());
        cardAccountRepository.save(cardAccount);

        Notification notification = Notification.builder()
                .admin(userRepository.findById((long) 1).get())
                .user(userRepository.findUserByCardAccountId(cardAccount.getId()))
                .message("Well done! Transaction successfully rejected." )
                .build();

        notificationService.addNotification(notification);

    }

    @Override
    public void withdraw(CardTransaction cardTransaction, CardAccount cardAccount) {
        BigDecimal withdrawMoney = cardTransaction.getAmount();
        BigDecimal cardMoneyBeforeWithdraw = cardAccount.getAccountBalance();

        if (withdrawMoney.compareTo(cardMoneyBeforeWithdraw) == 1) {
            throw new OperationFailedException("The balance of the account is not enough to complete the transaction․");
        }

        CardTransaction withdrawCardTransaction = new CardTransaction();
        withdrawCardTransaction.setAmount(withdrawMoney);
        withdrawCardTransaction.setAvailableBalance(cardAccount.getAccountBalance());
        withdrawCardTransaction.setDate(new Date());
        withdrawCardTransaction.setDescription("Withdraw from Card Account");
        withdrawCardTransaction.setStatus("Pending");
        withdrawCardTransaction.setCardAccount(cardAccount);
        withdrawCardTransaction.setType("Card");
        withdrawCardTransaction.setSenderAccountNumber("-");
        withdrawCardTransaction.setReceiverAccountNumber("-");

        cardTransactionRepository.save(withdrawCardTransaction);

        Notification notification = Notification.builder()
                .admin(userRepository.findById((long) 1).get())
                .user(currentUser.getCurrentUser())
                .message("Well done! The transaction is in the pending phase.")
                .build();

        notificationService.addNotification(notification);

    }

    @Override
    public void acceptWithdraw(CardTransaction cardTransaction) {

        CardAccount cardAccount = cardTransaction.getCardAccount();
        BigDecimal amount = cardTransaction.getAmount();

        if (amount.compareTo(cardAccount.getAccountBalance()) == 1) {
            cardTransaction.setStatus("Rejected");
            cardTransactionRepository.save(cardTransaction);
        } else {

            cardTransaction.setAvailableBalance(cardAccount.getAccountBalance().subtract(amount));
            cardTransaction.setStatus(cardTransaction.getStatus());
            cardTransactionRepository.save(cardTransaction);

            cardAccount.setAccountBalance(cardAccount.getAccountBalance().subtract(amount));
            cardAccountRepository.save(cardAccount);
        }

        Notification notification = Notification.builder()
                .admin(userRepository.findById((long) 1).get())
                .user(userRepository.findUserByCardAccountId(cardAccount.getId()))
                .message("Well done! Transaction successfully " + cardTransaction.getStatus() + " !")
                .build();

        notificationService.addNotification(notification);


    }

    @Override
    public void withdrawEdit(CardTransaction cardTransaction) {
        CardTransaction editedCardTransaction = cardTransactionRepository.findCardTransactionById(cardTransaction.getId());
        editedCardTransaction.setAmount(cardTransaction.getAmount());
        cardTransactionRepository.save(editedCardTransaction);
    }

    public void delete(Long id) {
        cardTransactionRepository.findById(id)
                .ifPresent(cardTransaction -> cardTransactionRepository.delete(cardTransaction));
    }
}

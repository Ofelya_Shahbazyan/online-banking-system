package am.online.banking.system.service;

import am.online.banking.system.bean.entity.PassiveAccount;

public interface PassiveAccountService {

    PassiveAccount createPassiveAccount();
}

package am.online.banking.system.service;

import am.online.banking.system.bean.dto.NotificationDto;

import java.util.List;

public interface NotificationResourceService {
    List<NotificationDto> getAllNotifications();
}

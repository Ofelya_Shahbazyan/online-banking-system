package am.online.banking.system.service.impl;

import am.online.banking.system.bean.dto.NotificationDto;
import am.online.banking.system.bean.entity.Notification;
import am.online.banking.system.bean.entity.User;
import am.online.banking.system.mapper.NotificationMapper;
import am.online.banking.system.repository.NotificationRepository;
import am.online.banking.system.service.NotificationService;
import am.online.banking.system.util.CurrentUser;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {

    private NotificationRepository notificationRepository;
    private NotificationMapper notificationMapper;
    private CurrentUser currentUser;

    public NotificationServiceImpl(NotificationRepository notificationRepository, NotificationMapper notificationMapper,
                                   CurrentUser currentUser) {
        this.notificationRepository = notificationRepository;
        this.notificationMapper = notificationMapper;
        this.currentUser = currentUser;
    }

    @Override
    public void addNotification(Notification notification) {
        notificationRepository.save(notification);
    }


    @Override
    public List<NotificationDto> getNotificationByRecipient() {
        User user = currentUser.getCurrentUser();
        List<Notification> notifications = notificationRepository.getNotificationByUser(user);
        return notificationMapper.mapAsList(notifications, NotificationDto.class);
    }

}


package am.online.banking.system.service.impl;

import am.online.banking.system.bean.entity.PassiveTransaction;
import am.online.banking.system.bean.entity.User;
import am.online.banking.system.repository.PassiveTransactionRepository;
import am.online.banking.system.service.PassiveTransactionService;
import am.online.banking.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PassiveTransactionServiceImpl implements PassiveTransactionService {


    private UserService userService;
    private PassiveTransactionRepository passiveTransactionRepository;

    @Autowired
    public PassiveTransactionServiceImpl(UserService userService, PassiveTransactionRepository passiveTransactionRepository) {
        this.userService = userService;
        this.passiveTransactionRepository = passiveTransactionRepository;
    }

    @Override
    public List<PassiveTransaction> findPassiveTransactionList(String username) {
        User user = userService.findUserByUsername(username);
       List<PassiveTransaction> passiveTransactionList = passiveTransactionRepository.findAllByPassiveAccount(user.getPassiveAccount());
        return passiveTransactionList;
    }

    @Override
    public void deposit(PassiveTransaction passiveTransaction) {

    }

    @Override
    public void depositEdit(PassiveTransaction passiveTransaction) {

    }

    @Override
    public void withdraw(PassiveTransaction passiveTransaction) {

    }

    @Override
    public void withdrawEdit(PassiveTransaction passiveTransaction) {

    }
}

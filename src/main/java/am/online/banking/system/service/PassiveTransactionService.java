package am.online.banking.system.service;

import am.online.banking.system.bean.entity.PassiveTransaction;

import java.util.List;

public interface PassiveTransactionService {

    List<PassiveTransaction> findPassiveTransactionList(String username);

    void deposit(PassiveTransaction passiveTransaction);

    void depositEdit(PassiveTransaction passiveTransaction);

    void withdraw(PassiveTransaction passiveTransaction);

    void withdrawEdit(PassiveTransaction passiveTransaction);
}

CREATE DATABASE IF NOT EXISTS `online_banking_system`;

USE online_banking_system;



--
-- ROLE
--

# DROP TABLE IF EXISTS role;
CREATE TABLE IF NOT EXISTS role(
    role_id  bigint(20) NOT NULL AUTO_INCREMENT,
    role_name   varchar(255) DEFAULT NULL,
    PRIMARY KEY (role_id)
)ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;


LOCK TABLES role WRITE;
/*!40000 ALTER TABLE role DISABLE KEYS */;
REPLACE INTO role
VALUES (1, 'ADMIN'),
       (2, 'USER');
/*!40000 ALTER TABLE role ENABLE KEYS */;
UNLOCK TABLES;



--
-- USER
--

# DROP TABLE IF EXISTS users;
CREATE TABLE  IF NOT EXISTS user(
                                    user_id bigint(20) NOT NULL AUTO_INCREMENT,
                                    email varchar(255) NOT NULL,
                                    first_name varchar(255) NOT NULL,
                                    is_account_non_expired bit(1) DEFAULT NULL,
                                    is_account_non_locked bit(1) DEFAULT NULL,
                                    is_credentials_non_expired bit(1) DEFAULT NULL,
                                    is_enabled bit(1) DEFAULT NULL,
                                    last_name varchar(255) NOT NULL,
                                    password varchar(255) NOT NULL,
                                    phone varchar(255) NOT NULL,
                                    username varchar(255) NOT NULL,
                                    card_account_account_id bigint(20) DEFAULT NULL,
                                    passive_account_account_id bigint(20) DEFAULT NULL,
                                    PRIMARY KEY (`user_id`),
                                    KEY card_account_id_fk  (card_account_account_id),
                                    KEY passive_account_id_fk  (passive_account_account_id),
									CONSTRAINT card_account_id_fk
									    FOREIGN KEY (passive_account_account_id)
									        REFERENCES passive_account (account_id),
									CONSTRAINT passive_account_id_fk
									    FOREIGN KEY (card_account_account_id)
									        REFERENCES card_account (account_id)
                                            ON DELETE CASCADE
                                            ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;


LOCK TABLES online_banking_system.user WRITE;
/*!40000 ALTER TABLE online_banking_system.user DISABLE KEYS */;
REPLACE INTO online_banking_system.user
VALUES ('1', 'shahbazyanofelya.9@gmail.com', 'Ofelya', true, true, true, true, 'Shahbazyan',
        '$2a$10$FTRgcZipE3AH1JujhZlHAu5jmHyagoFMW89rb8kW2U/k18JOk7L7i',
        '+374 90-000-000', 'admin', 1, 1);
/*!40000 ALTER TABLE online_banking_system.user ENABLE KEYS */;
UNLOCK TABLES;
# DELETE FROM online_banking_system.user
# WHERE user.user_id = '1';
# REPLACE INTO  online_banking_system.user
# VALUES ('1', 'shahbazyanofelya.9@gmail.com', 'Ofelya', true, true, true, true, 'Shahbazyan',  '$2a$10$FTRgcZipE3AH1JujhZlHAu5jmHyagoFMW89rb8kW2U/k18JOk7L7i',
#         '+374 90-000-000', 'admin', 1, 1);



--
-- USERS_ROLES
--

# DROP TABLE IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS  user_role (
                             user_id bigint(20) NOT NULL,
                             role_id bigint(20) NOT NULL,
                             PRIMARY KEY (user_id,role_id),
                             KEY role_id_fk  (role_id),
                             KEY user_id_fk (user_id),
                                            CONSTRAINT role_id_fk  FOREIGN KEY (role_id) REFERENCES role (role_id)
                                                ON DELETE CASCADE ON UPDATE CASCADE,
                                            CONSTRAINT user_id_fk FOREIGN KEY (user_id) REFERENCES user (user_id)
                                                ON DELETE CASCADE
                                                ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
# alter table user_role
#     add constraint FKa68196081fvovjhkek5m97n3y
#         foreign key (role_id)
#             references role (role_id);
#
# alter table user_role
#     add constraint FK859n2jvi8ivhui0rl0esws6o
#         foreign key (user_id)
#             references user (user_id);
#


# LOCK TABLES online_banking_system.user_role WRITE;
# /*!40000 ALTER TABLE online_banking_system.user_role DISABLE KEYS */;
# REPLACE INTO online_banking_system.user_role VALUES (1,1);
# /*!40000 ALTER TABLE online_banking_system.user_role ENABLE KEYS */;
# UNLOCK TABLES;
DELETE FROM online_banking_system.user_role
WHERE user_role.user_id = '1';
INSERT INTO  online_banking_system.user_role
VALUES (1, 1);



--
-- CARD_Account
--

CREATE TABLE IF NOT EXISTS card_account (
                                 account_id bigint(20) NOT NULL AUTO_INCREMENT,
                                 account_balance decimal(19,2) DEFAULT NULL,
                                 account_number bigint(20) DEFAULT NULL,
                                 PRIMARY KEY (account_id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;


LOCK TABLES card_account WRITE;
/*!40000 ALTER TABLE card_account DISABLE KEYS */;
REPLACE INTO card_account VALUES (1, 0.00, '1000000000000001');
/*!40000 ALTER TABLE card_account ENABLE KEYS */;
UNLOCK TABLES;
# DELETE FROM online_banking_system.card_accounts
# WHERE card_accounts.account_id = '1';
# REPLACE INTO card_accounts (account_id, account_balance, account_number)
# VALUES (1, 0.00, '1000000000000001');



--
-- Passive_Account
--

CREATE TABLE IF NOT EXISTS passive_account (
                                    account_id bigint(20) NOT NULL AUTO_INCREMENT,
                                    account_balance decimal(19,2) DEFAULT NULL,
                                    account_number bigint(20) DEFAULT NULL,
                                    PRIMARY KEY (account_id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ;

LOCK TABLES passive_account WRITE;
/*!40000 ALTER TABLE passive_account DISABLE KEYS */;
REPLACE INTO passive_account VALUES (1, 0.00, '7000000000000001');
/*!40000 ALTER TABLE passive_account ENABLE KEYS */;
UNLOCK TABLES;
# DELETE FROM online_banking_system.passive_accounts
# WHERE passive_accounts.account_id = '1';
# REPLACE INTO passive_accounts (account_id, account_balance, account_number)
# VALUES (1, 0.00, '7000000000000001');



--
-- CARD_Account_Transaction
--

CREATE TABLE IF NOT EXISTS card_transaction (
                                     transaction_id bigint(20) NOT NULL AUTO_INCREMENT,
                                     amount decimal(19,2) DEFAULT NULL,
                                     available_balance decimal(19,2) DEFAULT NULL,
                                     date datetime NOT NULL,
                                     description varchar(255) DEFAULT NULL,
                                     receiver_account_number varchar(255) NOT NULL,
                                     sender_account_number varchar(255) NOT NULL,
                                     status varchar(255) DEFAULT NULL,
                                     account_type varchar(255) DEFAULT NULL,
                                     card_account_id bigint(20) DEFAULT NULL,
                                     PRIMARY KEY (transaction_id),
                                     KEY card_account_id_fkey (card_account_id),
                                     CONSTRAINT card_account_id_fkey
                                         FOREIGN KEY (card_account_id)
                                             REFERENCES `card_accounts` (account_id)
                                             ON DELETE CASCADE
                                             ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;



--
-- Passive_Account_Transaction
--

CREATE TABLE IF NOT EXISTS online_banking_system.passive_transaction (
                                        transaction_id bigint(20) NOT NULL AUTO_INCREMENT,
                                        amount decimal(19,2) DEFAULT NULL,
                                        available_balance decimal(19,2) DEFAULT NULL,
                                        date datetime NOT NULL,
                                        description varchar(255) DEFAULT NULL,
                                        receiver_account_number varchar(255) NOT NULL,
                                        sender_account_number varchar(255) NOT NULL,
                                        status varchar(255) DEFAULT NULL,
                                        account_type varchar(255) DEFAULT NULL,
                                        passive_account_id bigint(20) DEFAULT NULL,
                                        PRIMARY KEY (transaction_id),
                                        KEY passive_account_id_fkey (passive_account_id),
                                        CONSTRAINT passive_account_id_fkey
                                            FOREIGN KEY (passive_account_id)
                                                REFERENCES passive_account (account_id)
                                                ON DELETE CASCADE
                                                ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;



--
-- Notification
--

CREATE TABLE IF NOT EXISTS notification (
                                 id bigint(20) NOT NULL AUTO_INCREMENT,
                                 created_date datetime DEFAULT NULL,
                                 message varchar(255) DEFAULT NULL,
                                 admin_id bigint(20) NOT NULL,
                                 user_id bigint(20) NOT NULL,
                                 PRIMARY KEY (id),
                                 KEY admin_id_fk  (admin_id),
                                 KEY user_id_fkey (user_id),
                                 CONSTRAINT admin_id_fk
                                     FOREIGN KEY (admin_id)
                                         REFERENCES user (user_id)
                                         ON DELETE CASCADE ON UPDATE CASCADE,
                                 CONSTRAINT user_id_fkey
                                     FOREIGN KEY (user_id)
                                         REFERENCES user (user_id)
                                         ON DELETE CASCADE
                                         ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
















